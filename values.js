const keys = require("./keys")

function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
    if(!(typeof obj==='object' && obj.constructor===Object)){
        return `Provide the proper input`
    }
    let values=[]
    for(let key in obj){
        values.push(obj[key])
    }
    return values
}

module.exports=values