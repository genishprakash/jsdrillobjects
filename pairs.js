function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
    if(!(typeof obj==='object' && obj.constructor===Object)){
        return `Provide the proper input`
    }
    const result=[]
    for(let key in obj){
        result.push([key,obj[key]])
    }
    return result

}
module.exports=pairs