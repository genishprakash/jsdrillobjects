function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
    if(!(typeof obj==='object' && obj.constructor===Object)){
        return `Provide the proper input`
    }
    for(let key in obj){
        obj[key]=cb(obj[key])
    }
    return obj
}

module.exports=mapObject