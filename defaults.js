function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
    if(!(typeof obj==='object' && obj.constructor===Object)){
        return `Provide the proper input`
    }
    for(let key in defaultProps){
        if(!(key  in obj)){
            obj[key]=defaultProps[key]
        }
    }
    return obj
}
module.exports=defaults